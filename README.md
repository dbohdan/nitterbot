# nitterbot

This IRC bot shows the author, date, and text of a tweet when a Twitter link is posted to an IRC channel it is on.  It also links to the tweet's image and (some types of) video attachments.  It does so using multiple public [Nitter](https://github.com/zedeus/nitter) instances.  Since instances may go offline and tend to get rate-limited by Twitter, it implements failover (several instances are tried per tweet) and backoff (an instance is ignored a certain number of times after failing).  It can wait for other Twitter bots to try the link first.

The Nitter client is packaged separately as a Tcl library in `nitter.tcl`.  `filter-instances.tcl` filters instance lists of one URL per line for apparently working instances.  This a standalone bot written in Tcl.  It is not compatible with [Eggdrop](https://en.wikipedia.org/wiki/Eggdrop).

## Requirements

### Debian/Ubuntu

```sh
sudo apt install -y expect make tcl tclcurl tcllib tcltls tdom tcl-thread
```

## License

MIT.
