#! /usr/bin/env tclsh
# A library and a CLI utility for accessing (a small part of) the Microsoft
# Azure Computer Vision Service API.
# Copyright (c) 2021 D. Bohdan and contributors listed in AUTHORS.
# License: MIT.
# To learn how to use this library, see the proc ::azure::cv::cli and the
# example https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts-sdk/image-analysis-client-library?pivots=programming-language-rest-api&tabs=visual-studio

package require json 1
package require TclCurl

namespace eval ::azure {
    namespace eval cv {
        variable dailyLimit 100
        variable key {}  ;# The API key for the service.
        variable quota {}
        variable region eastus
        variable timeout 30
        variable urlTemplate https://\$region.api.cognitive.microsoft.com/vision/v3.2/analyze?visualFeatures=Description
    }
}


proc ::azure::cv::caption imageURL {
    set resp [analyze $imageURL]
    if {[dict exists $resp error message]} {
        error [dict get $resp error message]
    }
    set captions [dict get $resp description captions]

    concat {*}[lmap cap $captions {
        list [dict get $cap text] [dict get $cap confidence]
    }]
}


proc ::azure::cv::analyze imageURL {
    variable key
    variable region
    variable urlTemplate

    set apiURL [string map [list {$region} $region] $urlTemplate]
    set payload [json::dict2json \
        [dict create url \
            [json::string2json $imageURL]]]

    set resp [json::json2dict [request $key $apiURL $payload]]

    return $resp
}


proc ::azure::cv::request {key apiURL payload} {
    variable timeout

    limit-requests

    try {
        set status [curl::transfer \
            -bodyvar json \
            -httpheader [list \
                {Content-Type: application/json} \
                "Ocp-Apim-Subscription-Key: $key"] \
            -post 1 \
            -postfields $payload \
            -timeout $timeout \
            -url $apiURL \
        ]

        return [encoding convertfrom utf-8 $json]
    } on error code {
        error [curl::easystrerror $code]
    }
}


proc ::azure::cv::limit-requests {} {
    variable dailyLimit
    variable quota

    set date [clock format [clock seconds] -format %Y-%m-%d]
    if {![dict exists $quota $date]} {
        dict set quota $date 0
    }

    if {[dict get $quota $date] >= $dailyLimit} {
        error [list daily request limit $dailyLimit reached]
    }

    dict incr quota $date

    return
}


proc ::azure::cv::cli url {
    variable key

    set envVar AZURE_CV_KEY

    if {![info exists ::env($envVar)]} {
        puts stderr [list Please set $envVar to your key]
        exit 1
    }
    set key $::env($envVar)

    foreach {cap p} [caption $url] {
        puts [format {%1.3f %s} $p $cap]
    }
}


# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    ::azure::cv::cli {*}$argv
}

package provide azure::cv 0
