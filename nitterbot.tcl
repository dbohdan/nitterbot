#! /usr/bin/env tclsh

package require Tcl 8.6-10

package require cmdline 1
package require tls 1

set auto_path [list [file dirname [info script]] {*}$auto_path]
package require azure::cv
package require nitter
package require picoirc

namespace eval nitterbot {
    variable done
}


oo::class create nitterbot::Bot {
    variable st

    constructor {
        done-var-name
        instances
        nick
        irc-url
        leader-bot-regexp
        delay
        no-delay-nick-regexp
        nickserv-identify
        describe-script
    } {
        my log nitterbot instances: $instances

        set st(connect-state) connecting
        set st(connect-delay) 5000
        set st(describe-script) ${describe-script}
        set st(done-var-name) ${done-var-name}
        set st(last-active) 0
        set st(last-message) {}
        set st(max-length-bytes) 400
        set st(max-shorter-bytes) 150
        set st(nick) $nick
        set st(nickserv-identify) ${nickserv-identify}
        set st(nitter) [nitter::Client new $instances -max-tries 5]
        set st(no-delay-nick-regexp) ${no-delay-nick-regexp}
        set st(recent-post-limit) 5
        set st(recent-post-time) 2000
        set st(recent-posts) {}
        set st(queue) {}
        set st(leader-regexp) ${leader-bot-regexp}
        set st(tick) 1000

        set st(delay) [expr { $st(leader-regexp) eq {} ? 0 : $delay }]

        if {[regexp \
                {^(ircs?://[^/]+)(?:/(.*))?$} \
                ${irc-url} \
                _ \
                st(server-url) \
                channels]} {
            set st(channels) [split $channels ,]
        } else {
            error [list invalid IRC URL: ${irc-url}]
        }

        # Derive the PicoIRC username from the nick.
        after [expr { $st(tick) * 2 }] \
            [list set ::tcl_platform(user) $::tcl_platform(user)]
        set ::tcl_platform(user) [my username]
        dict set picoirc::defaults realname nitterbot

        set st(context) [picoirc::connect \
            [list [self namespace] callback] \
            $nick \
            $st(server-url) \
        ]
    }

    destructor {
        $st(nitter) destroy
    }

    method username {} {
        string trim [regsub -all {[^A-Za-z0-9_-]} $st(nick) _] _
    }

    method callback {context state args} {
        set st(last-active) [clock milliseconds]

        if {[info exists st(context)] && $context ne $st(context)} {
            error [list expected context $st(context) but got $context]
        }

        my log $context $state {*}$args

        if {$st(connect-state) ne {ready}} {
            my initialize $context $state {*}$args
            return
        }

        switch -- $state {
            chat {}
            close {
                set $st(done-var-name) true
                return
            }
            default return
        }

        lassign $args channel nick message
        if {![regexp ^# $channel] || $nick eq $st(nick)} return

        set nitterError \
            {^[^A-Za-z0-9]*(?:502 Bad Gateway|403 Forbidden|Error.\|.nitter|\[)}
        my log DEBUG leader? [my leader? $nick] regexp $nitterError $message [regexp $nitterError $message]
        if {[my leader? $nick]} {
            if {![regexp $nitterError $message]} {
                dict set st(last-message) $channel [clock seconds]
            }

            return
        }

        foreach url [nitterbot::find-twitter-urls $message] {
            try {
                set nr [$st(nitter) get $url]
                set response [nitterbot::format-tweet $nr]
                set response [my add-image-description $response]

                my enqueue [dict create \
                    channel   $channel \
                    message   $response \
                    requester $nick \
                    timestamp [clock seconds] \
                ]
            } on error {res opts} {
                my log nitterbot error callback $res {*}$opts
            }
        }

        if {[llength $st(queue)] > 0} {
            my tick
        }
    }

    method add-image-description formatted {
        if {$st(describe-script) eq {}
            || ![regexp {Images: ([^ ]+)} $formatted _ imageURL]} {
            return $formatted
        }

        try {
            lassign [{*}$st(describe-script) $imageURL] cap conf

            if {$cap ne {}} {
                set replacement [format \
                    {%s (description: %s; confidence: %1.2f)} \
                    $imageURL \
                    $cap \
                    $conf \
                ]

                set formatted [string map \
                    [list $imageURL $replacement] \
                    $formatted \
                ]
            }
        } on error e {
            my log CV failed to describe image: $e
        }

        return $formatted
    }

    method initialize {context state args} {
        if {$state eq {debug}} {
            return
        }

        if {$state eq {connect}} {
            set st(connect-state) waiting-to-auth
            my ready-to-auth? $context
            return
        }

        if {$st(connect-state) eq {auth-sent}
            && $state eq {chat}
            && [lindex $args 1] eq {NickServ}
            && [regexp -nocase identified $args]} {
            set st(connect-state) auth-done
            # Do not return here.
        }

        if {$st(connect-state) eq {auth-done}} {
            picoirc::post \
                $context \
                {} \
                [list /join [join $st(channels) ,]] \

            set st(connect-state) ready
        }
    }

    method ready-to-auth? context {
        # Wait until there have been no messages for long enough.
        if {[clock milliseconds] - $st(last-active) < $st(connect-delay)} {
            after $st(tick) [self namespace] ready-to-auth? $context
            return
        }

        if {$st(nickserv-identify) eq {}} {
            set st(connect-state) auth-done
            my initialize $context dummy
        } else {
            picoirc::post \
                $context \
                {} \
                "/msg NickServ IDENTIFY $st(nickserv-identify)" \

            set st(connect-state) auth-sent
        }
    }

    method enqueue item {
        lappend st(queue) $item
    }

    method no-delay-nick? nick {
        nitterbot::match-nonempty-regexp $st(no-delay-nick-regexp) $nick
    }

    method post-queued {} {
        set keep {}
        set now [clock seconds]

        set len [llength $st(queue)]
        for {set i 0} {$i < $len} {incr i} {
            if {[my rate-limit?]} {
                lappend keep {*}[lrange $st(queue) $i end]
                break
            }

            set itemDict [lindex $st(queue) $i]
            array set item $itemDict

            # Discard items that have possibly been posted by another bot.
            if {[dict exists $st(last-message) $item(channel)]
                && $item(timestamp)
                   <= [dict get $st(last-message) $item(channel)]} {
                continue
            }

            # Reenqueue items too new to discard or post.
            if {![my no-delay-nick? $item(requester)]
                && $now - $item(timestamp) < $st(delay)} {
                lappend keep $itemDict
                continue
            }

            my post $st(context) $item(channel) $item(message)

            unset item
        }

        set st(queue) $keep
    }

    method rate-limit? {} {
        set now [clock milliseconds]
        set st(recent-posts) [lmap x $st(recent-posts) {
            if {$now - $x > $st(recent-post-time)} continue
            lindex $x
        }]

        expr {
            [llength $st(recent-posts)] >= $st(recent-post-limit)
        }
    }

    method post {context channel message} {
        set len [string length $message]

        set fragments [nitterbot::break-up \
            $message \
            $st(max-length-bytes) \
            $st(max-shorter-bytes) \
        ]
        foreach fragment $fragments {
            picoirc::post $st(context) $channel [string trimright $fragment]
            my add-recent-post
        }
    }

    method add-recent-post {} {
       lappend st(recent-posts) [clock milliseconds]
    }

    method log args {
        set timestamp [clock format \
            [clock seconds] \
            -format {%Y-%m-%d %H:%M:%S UTC} \
            -gmt 1 \
        ]
        puts [concat $timestamp $args]
    }

    method leader? nick {
        nitterbot::match-nonempty-regexp $st(leader-regexp) $nick
    }

    method tick {} {
        try {
            my post-queued

            if {[llength $st(queue)] > 0} {
                after $st(tick) [self namespace] tick
            }
        } on error {res opts} {
            my log nitterbot error tick $res {*}$opts
        }
    }
}


proc nitterbot::with-var {varName tempValue script} {
    upvar 1 $varName v
    if {[info exists v]} {
        set prev $v
    }

    set v $tempValue
    uplevel 1 $script

    if {[info exists prev]} {
        set v $prev
    } else {
        unset v
    }
}


proc nitterbot::sepsplit {str regexp {includeSeparators 1}} {
    if {$str eq {}} {
        return {}
    }
    if {$regexp eq {}} {
        return [split $str {}]
    }
    # Thanks to KBK for the idea.
    if {[regexp $regexp {}]} {
        return -code error \
               -errorcode {JIMLIB STRING SEPSPLIT INFINITE-LOOP}
               [list splitting on regexp $regexp would cause infinite loop]
    }

    # Split $str into a list of fields and separators.
    set fieldsAndSeps {}
    set offset 0
    while {[regexp -start $offset -indices -- $regexp $str match]} {
        lassign $match matchStart matchEnd
        lappend fieldsAndSeps \
                [string range $str $offset [expr {$matchStart - 1}]]
        if {$includeSeparators} {
            lappend fieldsAndSeps \
                    [string range $str $matchStart $matchEnd]
        }
        set offset [expr {$matchEnd + 1}]
    }
    # Handle the remainder of $str after all the separators.
    set tail [string range $str $offset end]
    if {$tail eq {}} {
        # $str ended on a separator.
        if {!$includeSeparators} {
            lappend fieldsAndSeps {}
        }
    } else {
        lappend fieldsAndSeps $tail
        if {$includeSeparators} {
            lappend fieldsAndSeps {}
        }
    }

    return $fieldsAndSeps
}


proc nitterbot::byte-length {encoding str} {
    string length [encoding convertto $encoding $str]
}


proc nitterbot::take-prefix {s maxLen {lengthCommand {byte-length utf-8}}} {
    set left 0
    set right [string length $s]

    while {$right - $left > 1} {
        set mid [expr { ($left + $right) / 2 }]
        set prefixLen [{*}$lengthCommand [string range $s 0 $mid]]

        if {$prefixLen <= $maxLen} {
            set left $mid
        } else {
            set right $mid
        }
    }

    list [string range $s 0 $left] [string range $s $left+1 end]
}


proc nitterbot::break-up {text maxFragLen {maxShorterLen 20} {sep \\s} {lengthCommand {byte-length utf-8}}} {
    if {$maxFragLen <= 0} {
        error [list maxFragLen must be positive, is $maxFragLen]
    }
    if {$maxShorterLen < 0} {
        error [list maxShorterLen must be non-negative, is $maxShorterLen]
    }

    if {[{*}$lengthCommand $text] <= $maxFragLen} {
        return [list $text]
    }

    set words [sepsplit $text $sep]
    set words [lmap {word sep} $words {
        lindex $word$sep
    }]

    set fragLen 0
    set fragment {}
    set fragments {}
    for {set i 0} {$i < [llength $words]} {incr i} {
        set word [lindex $words $i]
        set wordLen [{*}$lengthCommand $word]

        if {$fragLen + $wordLen > $maxFragLen} {
            set remainLen [expr { $maxFragLen - $fragLen }]
            set breakWord [expr {
                $remainLen > $maxShorterLen
                || $wordLen > $maxFragLen
            }]

            if {$breakWord} {
                lassign [take-prefix $word $remainLen $lengthCommand] prefix rest
                append fragment $prefix
                lset words $i $rest
                incr i -1
            }

            lappend fragments $fragment
            set fragment {}
            set fragLen 0

            if {$breakWord} {
                continue
            }
        }

        append fragment $word
        incr fragLen $wordLen
    }

    lappend fragments $fragment

    return $fragments
}


proc nitterbot::find-twitter-urls text {
    set re {https?:\/\/(?:mobile\.)?twitter\.com/[^/]+/status/[0-9]+}

    regexp -all -inline $re $text
}


proc nitterbot::format-tweet wrapped {
    set tweet [dict get $wrapped tweet]
    set instance [dict get $wrapped instance]

    set vws \[\n\x0b\r\x85\u2028\u2029\]+
    set result [format \
        {%s (%s, %s)} \
        [dict get $tweet fullname] \
        [dict get $tweet username] \
        [lindex [dict get $tweet date] 0] \
         \
    ]

    set content [regsub -all $vws [dict get $tweet content] { / }]
    if {$content ne {}} {
        set startPadding {}
        if {[regexp {^['"“”‘’„”«»]} $content]} {
            set startPadding { }
        }
        set endPadding {}
        if {[regexp {(?:['"“”‘’„”«»]|://[^ ]+)$} $content]} {
            set endPadding { }
        }

        append result ": “$startPadding$content$endPadding”"
    }

    foreach {key format} {
        images { Images: %s}
        videos { Videos: %s}
    } {
        set urls($key) [lmap x [dict get $tweet $key] {
            string cat $instance $x
        }]

        if {$urls($key) ne {}} {
            append result [format $format [join $urls($key) { / }]]
        }
    }

    if {[dict get $tweet video-overlay]} {
        set link [lindex [dict get $wrapped tried] end]
        append result " Watch video: $link"
    }

    set result
}


proc nitterbot::match-nonempty-regexp {exp string} {
    if {$exp eq {}} {
        return 0
    }

    regexp -- $exp $string
}


proc nitterbot::describe-image url {
    lrange [::azure::cv::caption $url] 0 1
}


proc nitterbot::main argv {
    set options {
        {attempts.arg
            3
            {How many times to try to reconnect if disconnected quickly}}
        {delay.arg 30 {How long to wait for a leader bot (seconds)}}
        {instances.arg {} {Nitter instance URLs}}
        {leader.arg {} {Twitter bot to wait for (regexp)}}
        {min-connected.arg
            60
            {What counts as a quick disconnection (seconds)}}
        {nick.arg nitterbot {Nick for the bot}}
        {no-delay-nick.arg {} {Nick the delay does not apply to (regexp)}}
        {connect.arg
            ircs://irc.libera.chat/##nitterbot-test
            {IRC URL to connect to}}
    }
    set usage "\[options]\noptions:"

    set error false
    try {
        array set params [::cmdline::getoptions argv $options $usage]
    } trap {CMDLINE USAGE} res {
        puts stderr $res
        exit 0
    }
    if {[llength $params(instances)] == 0} {
        puts stderr {no instances}
        exit 1
    }

    set attemptsStarting $params(attempts)
    set doneVar          ::nitterbot::done
    set minTimeConnected $params(min-connected)
    set nickServIdentify {}
    if {[info exists ::env(NITTERBOT_NICKSERV_IDENTIFY)]} {
        set nickServIdentify $::env(NITTERBOT_NICKSERV_IDENTIFY)
    }
    set describeScript {}
    if {[info exists ::env(NITTERBOT_AZURE_KEY)]} {
        set ::azure::cv::key $::env(NITTERBOT_AZURE_KEY)
        set describeScript nitterbot::describe-image
    }


    for {set attempts $attemptsStarting} {$attempts > 0} {} {
        set startTime [clock seconds]
        set $doneVar false

        set bot [nitterbot::Bot new \
            $doneVar \
            $params(instances) \
            $params(nick) \
            $params(connect) \
            $params(leader) \
            $params(delay) \
            $params(no-delay-nick) \
            $nickServIdentify \
            $describeScript \
        ]

        vwait $doneVar

        $bot destroy

        if {[clock seconds] - $startTime < $minTimeConnected} {
            incr attempts -1
        } else {
            set attempts $attemptsStarting
        }
    }
}


# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    nitterbot::main $argv
}

package provide nitterbot 0
