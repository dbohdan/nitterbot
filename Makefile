NITTERBOT_TCLSH ?= tclsh
NITTERBOT_CONNECT ?= 'ircs://irc.libera.chat/\#\#test'
NITTERBOT_LEADER ?= ''
NITTERBOT_NICK ?= nitterbot
NITTERBOT_NO_DELAY_NICK ?= ''
TEST_OPTIONS ?= -constraints internet

default: test


run:
	$(NITTERBOT_TCLSH) nitterbot.tcl \
	    --connect $(NITTERBOT_CONNECT) \
	    --instances "$$($(NITTERBOT_TCLSH) filter-instances.tcl --only-good --verbose instances | shuf | tr '\n' ' ')" \
	    --leader $(NITTERBOT_LEADER) \
	    --nick $(NITTERBOT_NICK) \
	    --no-delay-nick $(NITTERBOT_NO_DELAY_NICK) \


test: test-bot test-client

test-bot:
	$(NITTERBOT_TCLSH) nitterbot.test $(TEST_OPTIONS)

test-client:
	$(NITTERBOT_TCLSH) nitter.tcl $(TEST_OPTIONS)


.PHONY: default run test test-bot test-client
