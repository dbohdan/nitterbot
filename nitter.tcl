#! /usr/bin/env tclsh
# A library for getting the contents of tweets using Nitter instances.
# Copyright (c) 2021-2022 D. Bohdan and contributors listed in AUTHORS.
# License: MIT.

package require Tcl 8.6-10

package require fileutil 1
package require struct 2
package require uri 1
package require tdom 0.9
package require struct::list 1

namespace eval nitter {}


oo::class create nitter::Client {
    variable bump
    variable fetchScript
    variable sh
    variable timeout
    variable tries

    constructor {urls args} {
        set sh [nitter::Scheduler new [lmap url $urls {
            regsub /+$ $url {}
        }]]

        set bump 5
        nitter::set-option bump        -retry-delay

        set timeout 10
        nitter::set-option timeout     -timeout

        set tries 3
        nitter::set-option tries       -max-tries

        set fetchScript [list nitter::fetch-tclcurl $timeout]
        nitter::set-option fetchScript -fetch-script

        if {$args ne {}} {
            error [list unknown options: {*}$args]
        }
    }

    destructor {
        $sh destroy
    }

    method get twitterURL {
        set errors {}
        set tweet {}
        set tried {}

        for {set i 0} {$i < $tries} {incr i} {
            set instance {}

            try {
                set path [dict get [uri::split $twitterURL] path]
                set instance [$sh get]
                set url $instance/$path
                lappend tried $url

                set html [{*}$fetchScript $url]
                if {[regexp {Instance has been rate limited.} $html]} {
                    error {rate limited}
                }

                set tweet [nitter::parse $html]
            } on error {res opts} {
                lappend errors $res
                if {$instance ne {}} {
                    $sh bump $instance $bump
                }
                continue
            } on ok {} {
                break
            }
        }

        set code [expr { $tweet eq {} ? {error} : {ok} }]
        set result [dict create \
            errors $errors \
            instance $instance \
            tried $tried \
            tweet $tweet \
        ]
        return -code $code $result
    }
}


proc nitter::set-option {varName option} {
    upvar 1 args args $varName v

    if {![dict exists $args $option]} {
        return
    }

    set v [dict get $args $option]
    dict unset args $option
}


proc nitter::fetch-tclcurl {timeout url} {
    try {
        package present TclCurl
    } on error _ {
        package require TclCurl
    }

    try {
        set status [curl::transfer \
            -bodyvar html \
            -timeout $timeout \
            -url $url \
        ]

        return [encoding convertfrom utf-8 $html]
    } on error code {
        error [curl::easystrerror $code]
    }
}


proc nitter::parse html {
    set class {[contains(concat(" ",normalize-space(@class)," ")," %s ")]}

    set doc [dom parse -html $html]
    set root [$doc documentElement]

    set tweet [$root selectNodes [format //*$class main-tweet]]

    foreach field {fullname username} {
        set node [$tweet selectNodes [format (.//*$class)\[1\] $field]]
        dict set contents $field [$node asText]
    }

    set node [$tweet selectNodes {//meta[@property="og:description"]}]
    dict set contents content [$node getAttribute content]

    set node [$tweet selectNodes [format (.//*$class/a)\[1\] tweet-date]]
    set date [$node getAttribute title]
    dict set contents date [reformat-date $date]

    set node [$tweet selectNodes [format .//*$class video-overlay]]
    dict set contents video-overlay [expr { $node ne {} }]

    set attachments [$tweet selectNodes [format .//*$class attachments]]
    foreach {key query attr} {
        images .//a href
        videos .//video/source src
    } {
        dict set contents $key {}
        if {$attachments eq {}} continue

        set nodes [[lindex $attachments 0] selectNodes $query]
        set attrs [lmap node $nodes { $node getAttribute $attr }]
        dict set contents $key $attrs
    }

    $doc delete

    return $contents
}


proc nitter::reformat-date date {
    if {[regexp {(\d+)/(\d+)/(\d+), ([\d:]+)} $date _ day month year time]} {
        return [format {%04u-%02u-%02u %s} $year $month $day $time]
    }

    try {
        clock scan $date \
            -format {%b %d, %Y · %l:%M %p UTC} \
            -locale en_US \
            -timezone :Etc/UTC \
    } on error _ {
        # Do nothing.
    } on ok t {
        return [clock format $t \
            -format {%Y-%m-%d %H:%M:%S} \
            -timezone :Etc/UTC \
        ]
    }

    return $date
}


oo::class create nitter::Scheduler {
    variable counters
    variable cursor
    variable items
    variable n

    constructor _items {
        set cursor 0
        set items $_items
        set n [llength $items]

        foreach item $items {
            dict set counters $item 0
        }
    }

    method bump {item {n 1}} {
        if {$item ni $counters} {
            error [list unknown item: $item]
        }

        dict incr counters $item $n
    }

    method get {} {
        while true {
            set item [lindex $items $cursor]
            try {
                if {[dict get $counters $item] == 0} break
                dict incr counters $item -1
            } finally {
                my Next
            }
        }

        return $item
    }

    method try-item {varName errorBump body} {
        upvar 1 $varName item

        set item [my get]

        try {
            uplevel 1 $body
        } on error {res opts} {
            my bump $item $errorBump
        } on ok {res opts} {}

        return -options $opts $res
    }

    method Next {} {
        incr cursor
        if {$cursor == $n} { set cursor 0 }
    }
}


proc nitter::run-tests {} {
    package require tcltest 2

    set instanceListFile [file dirname [info script]]/instances
    set instances [struct::list shuffle [split \
        [string trim [fileutil::cat $instanceListFile]] \
        \n \
    ]]
    set tweets {
        first
        https://twitter.com/jack/status/20

        jp
        https://twitter.com/NHK_PR/status/1382967748444233729

        images
        https://twitter.com/akinecoco987/status/1382641290085486594

        embed
        https://twitter.com/Lawrence/status/1384198638377734148

        link
        https://twitter.com/jon_mellon/status/1385003434114527235
    }

    tcltest::test Scheduler-1.1 {} -body {
        set sh [Scheduler new {a b c}]
        $sh destroy
    } -result {}

    tcltest::test Scheduler-1.2 {} -body {
        set result {}
        set sh [Scheduler new {a b c}]

        foreach _ {1 2 3 4 5 6} {
            lappend result [$sh get]
        }
        $sh bump b 2
        foreach _ {1 2 3 4 5 6 7} {
            lappend result [$sh get]
        }

        $sh destroy
        set result
    } -result {a b c a b c a c a c a b c}

    tcltest::test Scheduler-1.3 {} -body {
        set sh [Scheduler new {a b c}]
        set result {}

        $sh try-item x 1 { lappend result $x }
        catch { $sh try-item x 1 { error fail } }
        foreach _ {1 2 3 4 5 6} {
            $sh try-item x 1 { lappend result $x }
        }

        $sh destroy
        set result
    } -result {a c a c a b c}


    tcltest::test Nitter-1.1 {} -body {
        set n [Client new http://nitter.net]
        $n destroy
    } -result {}

    tcltest::test Nitter-1.2 {} -constraints internet -body {
        set n [Client new $instances]
        set result [dict get [$n get [dict get $tweets first]] tweet]
        dict unset result fullname
        $n destroy

        set result
    } -match glob -result [list \
        username @jack \
        content {just setting up my twttr} \
        date {2006-03-21 20:50:??} \
        video-overlay 0 \
        images {} \
        videos {} \
    ]

    tcltest::test Nitter-1.3 {} -constraints internet -body {
        set n [Client new $instances]
        set result [dict get [$n get [dict get $tweets jp]] tweet fullname]
        $n destroy

        set result
    } -result NHK広報局

    tcltest::test Nitter-1.4 {} -constraints internet -body {
        set n [Client new $instances]
        set result [dict get [$n get [dict get $tweets images]] tweet images]
        $n destroy

        set result
    } -result [list \
       /pic/orig/media%2FEzAg6TkUcAIuDpz.jpg \
       /pic/orig/media%2FEzAg6nzVkAAseH8.jpg \
       /pic/orig/media%2FEzAg65gVcAIt2EI.jpg \
       /pic/orig/media%2FEzAg7OpVEAIgREy.jpg \
    ]

    tcltest::test Nitter-1.5 {} -constraints internet -body {
        set n [Client new $instances]
        set result [dict get [$n get [dict get $tweets embed]] tweet date]
        $n destroy

        set result
    } -match regexp -result {2021-04-19 17:34:0\d}

    tcltest::test Nitter-1.6 {} -constraints internet -body {
        set n [Client new $instances]
        set result [dict get [$n get [dict get $tweets link]] tweet content]
        $n destroy

        set result
    } -match glob -result *https://osf.io/preprints/socarxiv/9qj4f


    incr ::failed [expr {$tcltest::numTests(Failed) > 0}]
    tcltest::cleanupTests
}


# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    nitter::run-tests
    if {$failed > 0} {
        exit $failed
    }
}

package provide nitter 0
