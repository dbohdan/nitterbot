#! /usr/bin/env tclsh

package require Tcl 8.6-10

package require cmdline 1
package require defer
package require Thread 2


set threadScript {
    package require TclCurl

    proc check-instance url {
        set error {}
        set status -

        try {
            curl::transfer \
                -bodyvar html \
                -connecttimeout 5 \
                -nosignal 1 \
                -timeout 5 \
                -url $url \

            set status ?
            if {[regexp -nocase {"og:site_name" content="Nitter"} $html]} {
                set status +
            }
        } on error code {
            set error [curl::easystrerror $code]
        }

        return [list $url $status $error]
    }

    check-instance %s
}


proc filter-instances argv {
    lassign [parse-cli $argv] file mode

    if {$file in {{} -}} {
        set ch stdin
    } else {
        set ch [open $file]
    }
    defer::defer close $ch
    set instances [split [string trim [read $ch]] \n]

    set pool [tpool::create -maxworkers 20]
    set jobs [lmap instance $instances {
        tpool::post \
            $pool \
            [format $::threadScript [list $instance]] \
    }]

    while {$jobs ne {}} {
        foreach job [tpool::wait $pool $jobs jobs] {
            lassign [tpool::get $pool $job] instance status error

            if {{o} in $mode} {
                if {$status eq {+}} {
                    output $instance
                }
                continue
            }

            output "$status $instance"
            if {$status eq {-}} {
                output "  error: $error"
            }

        }
    }

}


proc parse-cli argv {
    set options {
        {o {Print only good instances}}
        {only-good -}
        {v {Print to stderr, too}}
        {verbose -}
    }
    set usage "\[options] \[file\]\noptions:"

    set error false
    try {
        array set params [cmdline::getoptions argv $options $usage]
    } trap {CMDLINE USAGE} res {
        puts -nonewline stderr $res
        exit 0
    }
    if {[llength $argv] > 1} {
        puts -nonewline stderr [cmdline::usage $options $usage]
        exit 1
    }

    set mode {}
    if-any-true params(o) params(only-good) {
        lappend mode o
    }
    if-any-true params(v) params(verbose) {
        lappend mode v
    }

    set path [lindex $argv 0]

    list $path $mode
}


proc if-any-true args {
    if {[llength $args] < 2} {
        error {wrong # args}
    }
    set varNames [lrange $args 0 end-1]
    set body [lindex $args end]

    foreach varName $varNames {
        upvar 1 $varName v
        if {[info exists v] && $v} {
            uplevel 1 $body
            return
        }
    }
}


proc output text {
    upvar 1 mode mode

    puts $text
    if {{v} in $mode} {
        puts stderr $text
    }
}


filter-instances $argv
