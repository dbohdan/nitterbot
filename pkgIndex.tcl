package ifneeded azure::cv 0 [list source [file join $dir azure-cv.tcl]]
package ifneeded nitter 0 [list source [file join $dir nitter.tcl]]
package ifneeded nitterbot 0 [list source [file join $dir nitterbot.tcl]]
package ifneeded picoirc 0.10.0 [list source [file join $dir vendor picoirc picoirc.tcl]]
